import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.details_page import DetailsPage
from pages.login_page import LoginPage
from pages.cockpit_page import CockpitPage
from pages.project_page import ProjectPage
from pages.add_project import AddProjectPage
from pages.utils import RandomUtil


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login_arena(LoginPage.login, LoginPage.password)
    yield browser
    browser.quit()


def test_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title')

    project_page = ProjectPage(browser)
    project_page.click_project()
    assert browser.find_element(By.CSS_SELECTOR, '#prefix')

    add_project_page = AddProjectPage(browser)
    project_prefix = RandomUtil.get_random_string(6)
    project_name = f"4_testers234 {project_prefix}"
    add_project_page.add_project(project_name, project_prefix)
    assert browser.find_element(By.CSS_SELECTOR, '#j_info_box').is_displayed()

    details_page = DetailsPage(browser)
    details_page.close_new_project_confirmation()
    details_page.click_cockpit()

    cockpit_page.click_administration()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == "Projekty"

    project_page.find_project(project_name)
    assert browser.find_element(By.CSS_SELECTOR, 'table > tbody tr:first-child > .t_number').text == project_prefix
